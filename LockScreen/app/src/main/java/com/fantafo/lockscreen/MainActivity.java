package com.fantafo.lockscreen;

import android.Manifest;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.PowerManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //                                    STATIC REQUEST_CODE
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    static final int REQUEST_CODE_ENABLE_ADMIN = 97;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //타이틀바 제거
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        /*
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true)
                    lockScreen();
            }
        }).start();*/
        lockScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // 퍼미션 획득을 검사하고, 검사가 실패했을 경우 실행을 중지한다.
        if (!RequestPermissions()) {
            Toast.makeText(this, "권한이 올바르지 않습니다. 모든 권한을 허용해 주시기 바랍니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        // 각 서비스들을 시작한다.
        //StartServices(this);
        //lockScreen();
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //                                    Permission Methods
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////////

    boolean RequestPermissions() {
        if (needDeviceAdministrator()) return false;
        return true;
    }

    /**
     * FimsAgent가 디바이스 관리자인지 확인하고 등록을 유도한다.
     */
    public boolean needDeviceAdministrator() {
        ComponentName cn = new ComponentName(this, FimsDeviceAdminReceiver.class);
        DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!dpm.isAdminActive(cn)) {
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, cn);
            startActivityForResult(intent, REQUEST_CODE_ENABLE_ADMIN);
            return true;
        }
        return false;
    }


    protected void lockScreen() {
        //화면이 켜져있다면 화면도 끈다.
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (pm.isInteractive()) {
            DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
            if (dpm != null) {
                dpm.lockNow();
            }
        }
    }
}
